use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn guessing_game() {
    println!("Guess the number!");

    let secret_number: i32 = rand::thread_rng().gen_range(0..=10);
    println!("The secret number is {secret_number}!");

    loop {
        println!("Please input your guess:");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: i32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guessed {guess}");

        match guess.cmp(&secret_number) {
            Ordering::Equal => {
                println!("You guessed correctly!");
                break;
            }
            Ordering::Greater => println!("Your guessed number is too big"),
            Ordering::Less => println!("Your guessed number is too small"),
        }
    }
}
