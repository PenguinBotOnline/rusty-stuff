// Constants
const GLOBAL_VARIABLE: &str = "Hello, World!";

fn main() {
    println!("{GLOBAL_VARIABLE}");

    // Immutable variable
    let x = 99;
    // Mutable variable
    let mut y = x;
    y = y - 1;
    println!("{y}");

    // Variable shadowing
    let spaces = "          ";
    let spaces = spaces.len();
    println!("{spaces}");
}
